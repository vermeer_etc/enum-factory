/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package com.mycompany.samples.enumfactory;

/**
 *
 * @author Yamashita,Takahiro
 */
public enum GenderTypeWithStrategy {
    MALE(0, new Male()),
    FEMALE(1, new Female());

    private final Integer genderCode;
    private final GenderInterface gender;

    private GenderTypeWithStrategy(Integer genderCode, GenderInterface gender) {
        this.genderCode = genderCode;
        this.gender = gender;
    }

    public Integer genderCode() {
        return this.genderCode;
    }

    public GenderInterface getGender() {
        return this.gender;
    }

}
