/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package com.mycompany.samples.enumfactory;

import java.util.Objects;

/**
 *
 * @author Yamashita,Takahiro
 */
public enum Sex {
    MAN(0, new Man()),
    WOMAN(1, new Woman());

    private final Integer sexCode;
    private final SexInterface sex;

    private Sex(Integer sexCode, SexInterface sex) {
        this.sexCode = sexCode;
        this.sex = sex;
    }

    public static SexInterface of(Integer sexCode, String name) {
        for (Sex sexType : Sex.values()) {
            if (Objects.equals(sexType.sexCode, sexCode)) {
                return sexType.sex.create(name);
            }
        }
        throw new IllegalArgumentException("sex code invalid");
    }

    public boolean isAssignableFrom(SexInterface sex) {
        return this.sex.getClass().isAssignableFrom(sex.getClass());
    }

}
