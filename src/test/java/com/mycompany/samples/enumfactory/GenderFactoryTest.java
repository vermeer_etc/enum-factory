/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package com.mycompany.samples.enumfactory;

import static org.hamcrest.CoreMatchers.is;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Yamashita,Takahiro
 */
public class GenderFactoryTest {

    @Test
    public void orthodoxFactoryMethod() {
        GenderFactory.create(GenderType.valueOf("MALE")).action();
        GenderFactory.create(GenderType.valueOf("FEMALE")).action();
    }

    @Test
    public void enumStrategyFactoryMethod() {
        GenderEnumStrategyFactory.create(0).action();
        GenderEnumStrategyFactory.create(1).action();
    }

    @Test
    public void enumFactoryMethod() {
        Gender.of(0).action();
        Gender.of(1).action();
    }

    @Test
    public void enumFactoryMethodWithParam() {
        Sex.of(0, "taro").action();
        Sex.of(1, "hanako").action();
    }

    @Test
    public void isAssignableFrom() {
        SexInterface man = Sex.of(0, "taro");
        SexInterface woman = Sex.of(1, "hanako");

        Assert.assertThat(Sex.MAN.isAssignableFrom(man), is(true));
        Assert.assertThat(Sex.WOMAN.isAssignableFrom(woman), is(true));
    }

}
